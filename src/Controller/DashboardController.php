<?php

namespace dlouhy\AdminTemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends Controller
{
	
	/**
	 * @Route("/dashboard", name="admin_dashboard")
	 */	
    public function listAction()
    {
        return $this->render('dlouhyAxessAdminBundle:Dashboard:index.html.twig');
    }
}
