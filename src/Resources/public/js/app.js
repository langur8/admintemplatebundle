$(document).ready(function () {

    ajaxForm.init();
    ajaxLink.init();

});


var webSite = {

    initDatepickers: function (locale) {

        $('.datepicker').datepicker({
            format: 'dd.mm.yyyy',
            weekStart: 1,
            //startDate: dd + '.' + mm + '.' + yyyy,
            autoclose: true,
            startView: 'month',
            minView: 'month',
            todayBtn: true,
            todayHighlight: true,
            keyboardNavigation: true,
            language: locale,
            forceParse: true,
            pickerPosition: 'top-left'
        });
    },

    removeTinyMCE: function () {
        if (typeof (tinymce) !== 'undefined') {
            var length = tinymce.editors.length;
            for (var i = length; i > 0; i--) {
                tinymce.editors[i - 1].remove();
            }

        }
    }

}
