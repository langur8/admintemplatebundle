var dataTableLoader = {
    dataTable: '',
    dataSet: [],
    dataSetTree: [],
    columns: [],
    order: [],
    columnFilter: [],
    dataTableSelector: '',
    language: {
        lengthMenu: "Zobrazit _MENU_ záznamů na stránku",
        zeroRecords: "Žádná data k zobrazení.",
        info: "Zobrazena stránka _PAGE_ z _PAGES_",
        infoEmpty: "Žádný záznam",
        infoFiltered: "(zobrazeno z celkového počtu _MAX_ záznamů)",
        search: "Vyhledat: ",
        paginate: {
            first: "První",
            previous: "Předchozí",
            next: "Další",
            last: "Poslední"
        }
    },
    loadDataTable: function (dataSet, columns, tree) {

        if (typeof tree === 'undefined') {
            tree = false;
        }

        if (this.dataTableSelector !== '') {
            var dataTableElement = $(this.dataTableSelector);
        } else {
            var dataTableElement = $('#data-table');
        }

        this.dataSet = dataSet;
        this.columns = columns;
        this.dataTable = dataTableElement.dataTable({
            language: dataTableLoader.language,
            data: dataTableLoader.dataSet,
            columns: dataTableLoader.columns,
            order: tree === false ? dataTableLoader.order : [],
            bSort: !tree,
            bPaginate: !tree,
            bInfo: !tree,
            stateSave: true,
            stateLoaded: function (settings, data) {
                //console.log(data);
            },
            fnDrawCallback: function (oSettings) {
                dataTableLoader.drawCallback(oSettings);
//				if(dataTableLoader.columnFilter.length !== 0 && this.api().data().toArray().length === 0) {
//					this.api().state.clear();
//					window.location.reload();
//				}
            },
            initComplete: function () {
                var table = this.api();
                if (dataTableLoader.columnFilter.length !== 0) {
                    for (var i = 0; i < dataTableLoader.columnFilter.length; i++) {
                        dataTableLoader.createColumnFilter(table, dataTableLoader.columnFilter[i][0], dataTableLoader.columnFilter[i][1])
                    }

                }
            }
        });
    },
    loadDataTableTree: function (dataSet, columns) {
        this.loadDataTable(dataSet, columns, true)
    },
    removeTinyEditors: function () {
        if (typeof (tinymce) !== 'undefined') {
            var length = tinymce.editors.length;
            for (var i = length; i > 0; i--) {
                tinymce.editors[i - 1].remove();
            }
            ;
        }
    },
    createColumnFilter: function (table, index, type) {

        if (type === 'select') {
            table.columns().eq(0).each(function () {
                var column = table.column(index);
                var select = $('<select style="width:100%" class="form-control input-sm" placeholder="Vyhledat..."><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? val : '', true, false)
                            .draw();
                    });

                var values = [];
                column.data().unique().sort().each(function (d, j) {
                    d = d.replace(/<div .*/gi, "").replace(/<(?:.|\n)*?>/gm, "")
                    if (values.indexOf(d) === -1 && d !== '') {
                        values.push(d);
                    }
                });

                var state = '';
                if (typeof(table.state()) !== null) {
                    state = table.state().columns[index].search.search.replace('\\', "");
                }
                for (var i = 0; i < values.length; i++) {
                    var checked = '';
                    if (state === values[i]) {
                        checked = 'selected="selected"'
                    }
                    select.append('<option value="' + values[i] + '" ' + checked + '>' + values[i] + '</option>')
                }


            });
        } else if (type === 'text') {
            var column = table.column(index);

            var state = '';
            if (typeof(table.state()) !== null) {
                state = table.state().columns[index].search.search;
            }

            $('<input class="form-control input-sm" style="width:100%" type="search" value="' + state + '" />')
                .appendTo($(column.footer()).empty())
                .on('keyup', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search(val ? '' + val + '' : '', true, false)
                        .draw();
                });

        }
    },

    drawCallback: function (oSettings) {
        $('#data-table tbody tr, #data-table-tree tbody tr').on({
            mouseenter: function () {
                $(this).children('td').children('div.btn-toolbox-wrapper').children('div.btn-toolbox').removeClass('hidden');
            },
            mouseleave: function () {
                $(this).children('td').children('div.btn-toolbox-wrapper').children('div.btn-toolbox').addClass('hidden');
            }
        });

//		$('#data-table .btn-active, #data-table-tree .btn-active').on('click', function (event) {
//			var obj = this;
//			event.preventDefault();
//			event.stopImmediatePropagation();
//
//			if ($(obj).data('ajax') == 'on') {
//
//				$(obj).fadeOut('fast', function() {
//					$(obj).parent().children('.spinner').show();
//				});
//				
//				$.ajax({
//					type: 'post',
//					url: obj.href,
//				})
//						.done(function (response) {
//							$(obj).toggleClass('btn-danger');
//							$(obj).toggleClass('btn-success');
//							if ($(obj).hasClass('btn-success')) {
//								$(obj).html('<i class="fa fa-check"></i> Ano');
//							} else {
//								$(obj).html('<i class="fa fa-close"></i> Ne');
//							}
//							$(obj).parent().children('.spinner').fadeOut('fast', function() {
//								$(obj).show();
//							});							
//							
//						});
//			} else {
//				document.location = obj.href;
//			}
//		});
    }
};