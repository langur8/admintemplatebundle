<?php

namespace dlouhy\AdminTemplateBundle\Menu\Matcher\Voter;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

/**
 * Voter based on the uri
 */
class UriVoter implements VoterInterface
{
    private $uri;

    public function __construct(RequestStack $requestStack)
    {
		if($requestStack->getCurrentRequest() instanceof Request) {		
			$this->uri = $requestStack->getCurrentRequest()->getBaseUrl() . $requestStack->getCurrentRequest()->getPathInfo();
		}
    }

    public function matchItem(ItemInterface $item)
    {		
        if (null === $this->uri || null === $item->getUri()) {
            return null;
        }
		
        if ($item->getExtra('match') !== null && strstr($this->uri, $item->getExtra('match'))) {
            return true;
        }

        return null;
    }
}