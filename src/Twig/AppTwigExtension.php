<?php

namespace dlouhy\AdminTemplateBundle\Twig;

use Doctrine\Common\Collections\Collection;

class AppTwigExtension extends \Twig_Extension
{

	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
			new \Twig_SimpleFilter('datetime', array($this, 'dateTimeFilter')),
			new \Twig_SimpleFilter('external_link', array($this, 'externalLinkFilter')),
			new \Twig_SimpleFilter('mn_list', array($this, 'mnListFilter')),
			new \Twig_SimpleFilter('bool', array($this, 'boolFilter')),
		);
	}


	public function getTests() {
		return array(
			new \Twig_SimpleTest('bool', array($this, 'boolTest')),
			new \Twig_SimpleTest('datetime', array($this, 'dateTimeTest')),
			new \Twig_SimpleTest('array', array($this, 'isArrayTest')),
		);
	}
	

//Filters
	public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
	{
		$price = number_format($number, $decimals, $decPoint, $thousandsSep);
		$price .= ' Kč';

		return $price;
	}
	

	public function dateTimeFilter(\DateTime $dateTime, $onlyDate = false) {
		if($onlyDate || $dateTime->format('His') === '000000') {
			return $dateTime->format('d.m.Y');
		} else {
			return $dateTime->format('d.m.Y H:i:s');
		}
		
	}
	
	public function externalLinkFilter($link) {
		if(substr($link, 0, 4) !== 'http') {
			$link = 'http://' . $link;
		}
		
		return $link;		
	}
	
	public function mnListFilter(Collection $array, $entity, $property) {
		$values = array();
		foreach($array->getIterator() as $item) {
			$getterE = 'get'.ucfirst($entity);
			$getterP = 'get'.ucfirst($property);
			
			$values[] = $item->$getterE()->$getterP();
		}
		
		return implode(', ', $values);
	}
	
	public function boolFilter($bool) {
		if($bool === true) {
			return 'Yes';
		}		
		return 'No';		
	}	
	
//Tests
	public function boolTest($value) {
		return is_bool($value);
	}

	
	public function dateTimeTest($value) {
		return $value instanceof \DateTime;
	}

	public function isArrayTest($value) {
		return is_array($value);
	}
		

	public function getName()
	{
		return 'app_twig_extension';
	}


}
